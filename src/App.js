import React, { useState } from 'react'
import io from 'socket.io-client'
import ReactPlayer from 'react-player'
import './App.css'

function App() {
  const [media, setMedia] = useState('https://www.youtube.com/watch?v=Snb2nao3egk')
  const [type, setType] = useState('video')
  const socket = io('http://localhost:5001')

  socket.on('media', (message) => {
    console.log('msg', message)
    const parsedMessage = JSON.parse(message)

    if (parsedMessage.type === 'video') {
      setMedia(`https://www.youtube.com/watch?v=${parsedMessage.media}`)
      setType('video')
      return
    }

    if (parsedMessage.type === 'img') {
      setType('img')
      setMedia('https://upload.wikimedia.org/wikipedia/en/thumb/8/88/Slovan_Bratislava_Logo.svg/1200px-Slovan_Bratislava_Logo.svg.png')
    }

  })

  return (
    <div className="App">
      <header className="App-header">
        {type === 'img' && <img src={media}></img>}
        {type === 'video' && <ReactPlayer url={media} muted={true} playing={true}/>}
      </header>
    </div>
  )
}

export default App
